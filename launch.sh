xhost +local:docker
CONTAINER_NAME=toptracker

docker run \
    --rm \
    --volume="$HOME/.Xauthority:/root/.Xauthority:rw"  \
    --volume /tmp/.X11-unix:/tmp/.X11-unix  \
    --volume "$HOME/.cache/$CONTAINER_NAME:/root/.config"  \
    --net=host  \
    --name $CONTAINER_NAME \
    -e DISPLAY=unix$DISPLAY \
    $CONTAINER_NAME 
