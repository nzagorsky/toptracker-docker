FROM ubuntu:latest

RUN apt-get update \
    && apt-get install -y libpulse-dev libqt5x11extras5 xvfb wget \
    && wget https://tracker-api.toptal.com/desktop/latest/debian -O /tmp/toptracker.deb \
    && dpkg -i /tmp/toptracker.deb; echo done \
    && apt-get install -fy

CMD [ "toptracker" ]
